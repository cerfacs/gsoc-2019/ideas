# pyTwiST : the Python Turbulence Statistics Toolkit

Validation of turbulent fields includes many pitfalls. Students usually start
from scratch using a turbulence textbook, meaning they must build the tools to
perform statistical analysis, implement known correlations, get all the
non-dimensionalization of the data right, remember to check local and global
conservation laws, etc... This back and forth between physics, statistics and
code usually yields many bugs before finally reaching a point where the turbulent
fields can indeed be validated.

In this project, we aim to gather code snippets written by students over the
years, and aggregate them into a small but clear, tested and robust library that
enables rapid validation of turbulence fields. The main applicaiton at CERFACS
is to validate turbulence fields that have been either compressed or generated
by deep learning approaches.


# FastLB : A fast Lattice Boltzmann fluid flow solver written in python, heavily
leveraging the Tensorflow library.

Fluid solvers have been traditionally written for CPU architectures, but the
advances in hardware driven by AI applications has led these codes to fall
behind in their capacity to leverage all the computing power available.

FastLB is an attempt de leverage the capacity of TensorFlow to perform linear
algebra operations efficiently on many platforms to effectively solve the fluid
flow in a Lattice Boltzmann context.